package com.example.desk.locprovider

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), LocationListener {
    private var locationManager: LocationManager? = null
    private var listProviders: List<String>? = null
    private val TAG = "LocationProvider"

    private var latitudeResult = 0.0
    private var longitudeResult = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //권한 체크
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val lastKnownLocation = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if (lastKnownLocation != null) {
            val lng = lastKnownLocation.longitude
            val lat = lastKnownLocation.latitude
            Log.d(TAG, "longtitude=$lng, latitude=$lat")
            tvGpsLatitude.text = java.lang.Double.toString(lat)
            tvGpsLongitude.text = java.lang.Double.toString(lng)
        }
        listProviders = locationManager!!.allProviders
        val isEnable = BooleanArray(3)
        for (i in listProviders!!.indices) {
            if (listProviders!![i] == LocationManager.GPS_PROVIDER) {
                isEnable[0] = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
                tvGpsEnable.text = ": " + isEnable[0].toString()

                locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
            }
        }

        Log.d(TAG, listProviders!![0] + '/'.toString() + isEnable[0].toString())
        Log.d(TAG, listProviders!![1] + '/'.toString() + isEnable[1].toString())
        Log.d(TAG, listProviders!![2] + '/'.toString() + isEnable[2].toString())
        button.setOnClickListener {
            if (latitudeResult!=0.0&&longitudeResult!=0.0){
                tvGpsLatitudeResult.text = java.lang.Double.toString(latitudeResult)
                tvGpsLongitudeResult.text= java.lang.Double.toString(longitudeResult)
            }

        }

    }

    override fun onProviderEnabled(provider: String) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
    }

    override fun onLocationChanged(location: Location) {
        var latitude = 0.0
        var longitude = 0.0
        if (location.provider == LocationManager.GPS_PROVIDER) {
            latitude = location.latitude
            longitude = location.longitude
            latitudeResult = latitude
            longitudeResult = longitude
        }
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

    }

    override fun onProviderDisabled(provider: String) {

    }

    override fun onStart() {
        super.onStart()
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //권한이 없을 경우 최초 권한 요청 또는 사용자에 의한 재요청 확인
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // 권한 재요청
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION), 100)
                return
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION), 100)
                return
            }
        }

    }

    override fun onPause() {
        super.onPause()
        locationManager!!.removeUpdates(this)
    }

    override fun onResume() {
        super.onResume()
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
    }
}
